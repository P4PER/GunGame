package me.paper.gungame;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import me.paper.gungame.commands.GUNGAME_Command;
import me.paper.gungame.events.BlockBreakListener;
import me.paper.gungame.events.BlockPlaceListener;
import me.paper.gungame.events.CreatureSpawnListener;
import me.paper.gungame.events.FoodLevelChangeListener;
import me.paper.gungame.events.InventoryClickListener;
import me.paper.gungame.events.PlayerDeathListener;
import me.paper.gungame.events.PlayerDropListener;
import me.paper.gungame.events.PlayerJoinListener;
import me.paper.gungame.events.PlayerPickUpListener;
import me.paper.gungame.events.PlayerQuitListener;
import me.paper.gungame.events.PlayerRespawnListener;

import org.bukkit.plugin.java.JavaPlugin;

public class GunGame extends JavaPlugin {
	
	public String pr = "�5GunGame �f> �7";
	public Map<UUID, GunPlayer> player = new HashMap<>();

	@Override
	public void onDisable() {
		
		System.out.println("GunGame > Plugin disabled!");
		
	}
	
	@Override
	public void onEnable() {
		
		System.out.println("GunGame > Plugin enabled!");
		
		registerListener();
		
		new GUNGAME_Command(this);
		
	}
	
	public void registerListener(){
		
		new PlayerDeathListener(this);
		new PlayerJoinListener(this);
		new PlayerRespawnListener(this);
		new FoodLevelChangeListener(this);
		new CreatureSpawnListener(this);
		new PlayerDropListener(this);
		new PlayerPickUpListener(this);
		new BlockBreakListener(this);
		new BlockPlaceListener(this);
		new PlayerQuitListener(this);
		new InventoryClickListener(this);
		
	}

}
