package me.paper.gungame;

import org.bukkit.entity.Player;

public class GunPlayer {
	
	Player p;
	Integer level = 0;
	
	GunGame plugin;
	
	public GunPlayer(Player p, GunGame plugin){
		this.p = p;
		this.plugin = plugin;
	}
	
	public Player getPlayer(){
		return p;
	}
	
	public void addLevel(){
		level++;
	}
	
	public Integer getLevel(){
		return level;
	}
	
	public void clearLevel(){
		if(level != 0) level = 0;
	}
	
	public void setNewKit(){
		
		if(level > 8) level = 0;
		
		EnumWeapons weapon = EnumWeapons.getByNumber(level);
		p.getInventory().setItem(0, weapon.getItem());

		p.sendMessage(level != 0 ? plugin.pr + "Du hast ein �cneues Kit �7erhalten. (�4" + getLevel() + "�7)" : plugin.pr + "Du bist nun wieder am �cAnfang�7.");
		p.setLevel(level);
		
	}
	
}
