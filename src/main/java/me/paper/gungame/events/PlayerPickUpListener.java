package me.paper.gungame.events;

import me.paper.gungame.GunGame;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.plugin.Plugin;

public class PlayerPickUpListener implements Listener {
	
	GunGame plugin;
	
	public PlayerPickUpListener(GunGame plugin){
		this.plugin = plugin;
		Bukkit.getPluginManager().registerEvents(this, (Plugin) plugin);
	}
	
	@EventHandler
	public void onPlayerDrop(PlayerPickupItemEvent e){
		
		e.setCancelled(true);
		
	}

}
