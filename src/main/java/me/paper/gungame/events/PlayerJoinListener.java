package me.paper.gungame.events;

import me.paper.gungame.GunGame;
import me.paper.gungame.GunPlayer;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.Plugin;

public class PlayerJoinListener implements Listener {
	
	GunGame plugin;
	
	public PlayerJoinListener(GunGame plugin){
		this.plugin = plugin;
		Bukkit.getPluginManager().registerEvents(this, (Plugin) plugin); 
	}
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e){
		
		Player p = e.getPlayer();
		GunPlayer gp = new GunPlayer(p, plugin);
		
		p.setExp(0);
		p.setHealth(20.0);
		p.getInventory().clear();
		gp.setNewKit();
		
		plugin.player.put(p.getUniqueId(), gp);
		e.setJoinMessage(null);
		
		if(!plugin.getConfig().contains("spawn")){
			p.sendMessage(plugin.pr + "Du hast den Spawn noch nicht gesetzt. Bitte setzte ihn mit �6\"/gungame setspawn\" �7.");
			return;
		}
		
		Location spawn = (Location) plugin.getConfig().get("spawn");
		p.teleport(spawn);
		
	}

}
