package me.paper.gungame.events;

import me.paper.gungame.GunGame;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.plugin.Plugin;

public class PlayerDropListener implements Listener {
	
	GunGame plugin;
	
	public PlayerDropListener(GunGame plugin){
		this.plugin = plugin;
		Bukkit.getPluginManager().registerEvents(this, (Plugin) plugin);
	}
	
	@EventHandler
	public void onPlayerDrop(PlayerDropItemEvent e){
		
		e.setCancelled(true);
		
	}

}
