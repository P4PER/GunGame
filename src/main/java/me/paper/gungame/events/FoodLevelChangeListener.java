package me.paper.gungame.events;

import me.paper.gungame.GunGame;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.plugin.Plugin;

public class FoodLevelChangeListener implements Listener {
	
	GunGame plugin;
	
	public FoodLevelChangeListener(GunGame plugin){
		this.plugin = plugin;
		Bukkit.getPluginManager().registerEvents(this, (Plugin) plugin);
	}
	
	@EventHandler
	public void onFoodChange(FoodLevelChangeEvent e){
		
		e.setCancelled(true);
		
	}

}
