package me.paper.gungame.events;

import me.paper.gungame.GunGame;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.plugin.Plugin;

public class InventoryClickListener implements Listener {
	
	GunGame plugin;
	
	public InventoryClickListener(GunGame plugin){
		this.plugin = plugin;
		Bukkit.getPluginManager().registerEvents(this, (Plugin) plugin);
	}
	
	@EventHandler
	public void onInventoryClick(InventoryClickEvent e){
		
		e.setCancelled(true);
		
	}

}
