package me.paper.gungame.events;

import me.paper.gungame.GunGame;
import me.paper.gungame.GunPlayer;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.plugin.Plugin;

public class PlayerRespawnListener implements Listener {
	
	GunGame plugin;
	
	public PlayerRespawnListener(GunGame plugin){
		this.plugin = plugin;
		Bukkit.getPluginManager().registerEvents(this, (Plugin) plugin);
	}
	
	@EventHandler
	public void onPlayerRespawn(PlayerRespawnEvent e){
		
		Player p = e.getPlayer();
		
		Location spawn = (Location) plugin.getConfig().get("spawn");
		e.setRespawnLocation(spawn);
		
		GunPlayer gp = plugin.player.get(p.getUniqueId());
		gp.clearLevel();
		gp.setNewKit();
		
	}

}
