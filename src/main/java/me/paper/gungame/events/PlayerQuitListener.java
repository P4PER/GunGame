package me.paper.gungame.events;

import me.paper.gungame.GunGame;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.Plugin;

public class PlayerQuitListener implements Listener {
	
	GunGame plugin;
	
	public PlayerQuitListener(GunGame plugin){
		this.plugin = plugin;
		Bukkit.getPluginManager().registerEvents(this, (Plugin) plugin);
	}
	
	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent e){
		
		e.setQuitMessage(null);
		
		plugin.player.remove(e.getPlayer().getUniqueId());
		
	}

}
