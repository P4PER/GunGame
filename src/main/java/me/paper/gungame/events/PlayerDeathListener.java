package me.paper.gungame.events;

import me.paper.gungame.GunGame;
import me.paper.gungame.GunPlayer;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class PlayerDeathListener implements Listener {
	
	GunGame plugin;
	
	public PlayerDeathListener(GunGame plugin){
		this.plugin = plugin;
		Bukkit.getPluginManager().registerEvents(this, (Plugin) plugin); 
	}

	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent e){
		
		Player killer = e.getEntity().getKiller();
		Player p = e.getEntity();
		
		e.setDeathMessage(null);
		
		e.setDroppedExp(0);
		e.getDrops().clear();
		p.getInventory().clear();
		
		killer.playSound(killer.getEyeLocation(), Sound.LEVEL_UP, 20, 20);
		killer.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 20 * 6, 2));
		
		GunPlayer gp = plugin.player.get(killer.getUniqueId());
		gp.addLevel();
		gp.setNewKit();
		
	}
	
}
