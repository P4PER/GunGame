package me.paper.gungame.events;

import me.paper.gungame.GunGame;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.plugin.Plugin;

public class CreatureSpawnListener implements Listener {
	
	GunGame plugin;
	
	public CreatureSpawnListener(GunGame plugin){
		this.plugin = plugin;
		Bukkit.getPluginManager().registerEvents(this, (Plugin) plugin);
	}
	
	@EventHandler
	public void onCreatureSpawn(CreatureSpawnEvent e){
		
		e.setCancelled(true);
		
	}

}
