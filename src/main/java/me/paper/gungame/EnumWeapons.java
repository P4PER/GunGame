package me.paper.gungame;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public enum EnumWeapons {
	
	WOOD_AXE(0, new ItemStack(Material.WOOD_AXE)),
	WOOD_SWORD(1, new ItemStack(Material.WOOD_SWORD)),
	STONE_AXE(2, new ItemStack(Material.STONE_AXE)),
	STONE_SWORD(3, new ItemStack(Material.STONE_SWORD)),
	IRON_AXE(4, new ItemStack(Material.IRON_AXE)),
	IRON_SWORD(5, new ItemStack(Material.IRON_SWORD)),
	DIAMOND_AXE(6, new ItemStack(Material.DIAMOND_AXE)),
	DIAMOND_SWORD(7, new ItemStack(Material.DIAMOND_SWORD)),
	FISHING_ROD(8, new ItemStack(Material.FISHING_ROD));
	
	Integer level;
	ItemStack stack;
	
	private EnumWeapons(Integer level, ItemStack stack) {
		this.level = level;
		this.stack = stack;
	}
	
	public Integer getLevel() {
		return level;
	}
	
	public ItemStack getItem(){

		ItemMeta meta = stack.getItemMeta();
		meta.spigot().setUnbreakable(true);
		stack.setItemMeta(meta);
		
		return stack;
	}
	
	public static EnumWeapons getByNumber(Integer number){
		
		for(EnumWeapons w : EnumWeapons.values()){
			
			if(number == w.getLevel()) return w;
			
		}
		
		return null;
		
	}

}
