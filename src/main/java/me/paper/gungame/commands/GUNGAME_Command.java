package me.paper.gungame.commands;

import me.paper.gungame.GunGame;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class GUNGAME_Command implements CommandExecutor {
	
	GunGame plugin;
	
	public GUNGAME_Command(GunGame plugin){
		this.plugin = plugin;
		Bukkit.getPluginCommand("GunGame").setExecutor(this);
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		
		if(!(sender instanceof Player)) return false;
		
		Player p = (Player) sender;
		
		if(args.length == 0){
			
			p.sendMessage("�7---------------- �6Help �7----------------");
			p.sendMessage("�6/gungame setspawn �f- �7Set the spawnpoint");
			p.sendMessage("�7------------------------------------------");
			
		}
		
		if(args.length == 1){
			
			if(args[0].equalsIgnoreCase("setspawn")){
				
				plugin.getConfig().set("spawn", p.getLocation());
				plugin.saveConfig();
				
				p.sendMessage(plugin.pr + "�aDu hast den Spawn erfolgreich gesetzt!");
				
			}
			
		}
		
		return false;
		
	}

}
